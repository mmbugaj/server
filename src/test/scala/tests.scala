import COMMUNICATION.RESULT.RESULT_SUCCESS
import COMMUNICATION._
import actors.{PhysicalDeviceConnector, DeviceManager}
import akka.actor.{Actor, ActorRef, ActorSystem, PoisonPill, Props}
import akka.contrib.pattern.ReceivePipeline
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.io.Tcp.{Received, Register, Write}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import akka.util.ByteString
import controllers.WebServer
import org.scalatest._
import spray.json.DefaultJsonProtocol._
import spray.json.{JsNumber, _}
import util.ResponsibilityChain._

import scala.concurrent.Await
import scala.concurrent.duration._

class IntegrationTest extends WordSpec with Matchers with ScalatestRouteTest with GivenWhenThen {
  def fixture = new {
    val connection = TestProbe()
    val manager = system.actorOf(DeviceManager.props)
    system.actorOf(PhysicalDeviceConnector.props(connection.ref, manager))
    val server = new WebServer(manager, system, materializer)
  }

  def createRegisterReq(_name: String, _id: Int, _channels: (Int, String)*): ANY_MESSAGE = {
    val channels = _channels.map(i => PWM_CHANNEL_INFO(i._2, i._1))
    val tag = TRANSACTION_TAG(1)
    val registerRequest = REGISTER_REQUEST(
      id = 1,
      description = None,
      name = "Controller",
      channelList = channels)
    ANY_MESSAGE(transactionTag = tag, registerRequest = Some(registerRequest))
  }

  def createPositiveAck(_transactionTag : Int) : ANY_MESSAGE = {
    ANY_MESSAGE(TRANSACTION_TAG(_transactionTag), ack = Some(ACK(RESULT_SUCCESS, Some(""))))
  }

  def sendToApp(msg: ANY_MESSAGE, stream: ActorRef): Unit = {
    val inByteString = ByteString(msg.toByteArray)
    stream ! Received(ByteString(inByteString.length.toByte) ++ inByteString)
  }

  "The server" when {
    "no devices are connected" should {
      val f = fixture
      import f._

      "return empty json array on /devices" in {
        Get("/devices") ~> server.route ~> check {
          responseAs[JsValue].convertTo[JsArray] shouldEqual JsArray()
        }
      }
    }

    "one device connects with two PWM controllers" should {
      val f = fixture
      import f._

      val app = connection.expectMsgPF() {
        case Register(handler, _, _) => handler
      }

      val deviceName1 = "asdf"
      val deviceName2 = "fdsa"
      sendToApp(createRegisterReq("", 0, (15, deviceName1), (10, deviceName2)), app)

      var devices : IndexedSeq[Int] = null
      "return one element in json array on /devices" in {
        Get("/devices") ~> server.route ~> check {
          devices = responseAs[JsValue].convertTo[JsArray].elements.map(_.convertTo[Int])
          devices should have size 2
        }
      }

      "return correct PWM controller description on /device/id" in {
        Get("/device/" + devices(0)) ~> server.route ~> check {
          responseAs[JsValue].asJsObject shouldEqual JsObject(
            "type" -> JsString("PWM"),
            "name" -> JsString(deviceName1)
          )
        }

        Get("/device/" + devices(1)) ~> server.route ~> check {
          responseAs[JsValue].asJsObject shouldEqual JsObject(
            "type" -> JsString("PWM"),
            "name" -> JsString(deviceName2)
          )
        }
      }

      "return js null when pwm channel has not been set" in {
        Get("/device/" + devices(0) + "/state") ~> server.route ~> check {
          responseAs[JsValue] shouldEqual JsObject(
            "value" -> JsNull
          )
        }
      }

      "succeed with setting both PWM channels" in {
        When("HTTP PUT requests are done")
        val result1 = Put("/device/" + devices(0) + "/state",  JsObject("value" -> JsNumber(10))) ~> server.route
        val result2 = Put("/device/" + devices(1) + "/state",  JsObject("value" -> JsNumber(4))) ~> server.route

        case class Key(val dutyCycle : Int, val physicalId : Int)
        case class Value(val persistentId : Int, result : RouteTestResult)

        def con1(expected : Map[Key, Value]) : Assertion = {
          Then("DEVICE actor receives order to set PWM channel")
          val any = ANY_MESSAGE.parseFrom(connection.expectMsgPF() {
            case Write(byteString, _) =>
              assert(byteString.tail.size == byteString.head.toInt)
              byteString.tail.toArray
          })
          val set = any.pwmSetChannelRequest.get
          val key = Key(set.dutyCycle, set.id)

          expected.get(key) match {
            case Some(Value(persistentId, result)) =>
              sendToApp(createPositiveAck(any.transactionTag.transactionTag), app)

              And("HTTP 200 response is received")
              result ~> check {
                status.intValue shouldBe 200
              }

              And("Recently set PWM channel should be reported on HTTP get")
              Get("/device/" + persistentId + "/state") ~> server.route ~> check {
                responseAs[JsValue] shouldEqual JsObject("value" -> JsNumber(set.dutyCycle))
              }

              if (expected.size == 1) succeed
              else con1(expected - key)

            case None => fail
          }
        }

        con1(Map(
          Key(10, 15) -> Value(devices(0), result1),
          Key(4, 10) -> Value(devices(1), result2)))
      }
    }
  }
}

class DelegateTest() extends TestKit(ActorSystem("DelegateTest"))
  with WordSpecLike with Matchers with ImplicitSender {

  case class A()
  case class B()
  class M extends Actor with UsesDelegates {
    override def receive: Receive = {
      case m @ (A() | B()) => passToDelegates(m)
    }
  }

  val probe = TestProbe()

  class Handles(accept : (Any) => Boolean) extends Actor with ReceivePipeline with Delegate with PassUnhandled {
    override protected[this] val pass: ActorRef = probe.ref
    override def receive: Receive = {
      case m if accept(m) => sender ! m
    }
  }

  val a1 = system.actorOf(Props(new Handles(_.isInstanceOf[A])))
  val b1 = system.actorOf(Props(new Handles(_.isInstanceOf[B])))
  val m = TestActorRef(new M)

  "Adding A delegate succeeds" in {
    noException should be thrownBy Await.result(m.underlyingActor.addDelegate(a1), 1 second)
  }

  "A is handled when A delegate is attached" in {
    m ! A()
    expectMsg(A())
  }

  "B is not handled when only A delegate is attached" in {
    m ! B()
    probe.expectMsg(B())
  }

  "Adding second delegate suceeds" in {
    noException should be thrownBy Await.result(m.underlyingActor.addDelegate(b1), 1 second)
  }

  "A and B are handled when A and B delegates are attached" in {
    m ! A()
    expectMsg(A())
    m ! B()
    expectMsg(B())
  }

  "Cannot add the same delegate second time" in {

  }

  "When delegate is stopped it is no more used in the chain" in {
    a1 ! PoisonPill
    awaitCond({
      m ! A()
      A() == probe.receiveOne(0 second)
    })
  }
}
