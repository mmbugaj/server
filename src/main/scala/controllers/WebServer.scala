package controllers

import actors.DeviceActor
import actors.DeviceActor._
import actors.DeviceManager.InterfaceToWebServer._
import actors.PWMChannelActor.InterfaceToDeviceManager._
import akka.actor.{ActorRef, ActorSystem, Status}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives._
import akka.pattern._
import akka.stream.ActorMaterializer
import akka.util.Timeout
import controllers.WebServer.InterfaceToDeviceManager.{GetDevicesResponse, NoSuchDevice}
import spray.json._

import scala.concurrent.duration._
import scala.util.{Failure, Success}

class WebServer(val manager : ActorRef,
                implicit val system : ActorSystem,
                implicit val materializer: ActorMaterializer) {
  implicit val executionContext = system.dispatcher
  implicit val timeout: Timeout = Timeout(5 second)

  import DefaultJsonProtocol._

  val route =
    path("devices") {
      get {
        onComplete(manager ? GetDevicesRequest()) {
          case Success(GetDevicesResponse(devices)) => complete(devices.toJson)
          case Success(Status.Failure(f)) => failWith(f) // kind of response from pipeTo pattern
          case Failure(f) => failWith(f)
        }
      }
    }  ~ path("device" / IntNumber) { id =>
      get {
        onComplete(manager ? RouteCommand(GetPropertiesInJsonReq(), id)) {
          case Success(GetPropertiesInJsonResp(jsValue)) => complete(jsValue)
          case Success(Status.Failure(f)) => failWith(f)
          case Failure(f) => failWith(f)
        }
      }
    } ~ path("device" / IntNumber / "state") { deviceId =>
      get {
        onComplete(manager ? RouteCommand(GetStateInJsonReq(), deviceId)) {
          case Success(GetStateInJsonResp(jsValue)) => complete(jsValue)
          case Success(Status.Failure(f)) => failWith(f)
          case Failure(f) => failWith(f)
        }
      } ~ put {
        entity(as[JsValue]) { jsValue =>
          onComplete(manager ? RouteCommand(SetStateFromJsonReq(jsValue), deviceId)) {
            case Success(SetStateFromJsonResp(status)) => complete(status.toString) // TODO
            case Success(Status.Failure(f)) => failWith(f)
            case Failure(f) => failWith(f)
          }
        }
      }
    }
}

object WebServer {
  object InterfaceToDeviceManager {
    case class GetDevicesResponse(devices : Iterable[DeviceActor.Id])
    case class NoSuchDevice()
  }
}
