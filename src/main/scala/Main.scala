import actors.{DeviceListener, DeviceManager}
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import controllers.WebServer

object Main extends App {
  Console.println("Hello World: " + (args mkString ", "))

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  val manager = system.actorOf(DeviceManager.props)
  val listener = system.actorOf(DeviceListener.props(manager))
  val server = new WebServer(manager, system, materializer)
  val bindingFuture = Http().bindAndHandle(server.route, "localhost", 8080)
}
