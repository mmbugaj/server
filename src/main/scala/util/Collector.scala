package util

import akka.util.ByteString
import play.api.Logger

final class Collector(callback : (ByteString) => Unit, expected : Int, upToDateBuffer : ByteString)
{
  def apply(data: ByteString) : Collector =
  {
    if (expected == 0)
      assert(upToDateBuffer.isEmpty)

    val (length, remaining) =
      if (expected == 0)
        readFromByteStream(data)
      else
        readFromByteStream(expected.toByte +: upToDateBuffer ++: data)

    new Collector(callback, length, remaining)
  }

  def readFromByteStream(alignedToLengthField : ByteString) : (Int, ByteString) =
  {
    val expected = alignedToLengthField.head
    val data = alignedToLengthField.drop(1)
    val dataLength = data.length

    if (expected <= dataLength) {
      callback(data.take(expected))

      if (expected == dataLength)
        (0, ByteString())
      else
        readFromByteStream(data.drop(expected))
    } else {
      (expected, data)
    }
  }
}
