package util.ResponsibilityChain

import akka.actor.{Actor, ActorRef, Props, Terminated, UnhandledMessage}
import akka.contrib.pattern.ReceivePipeline
import akka.contrib.pattern.ReceivePipeline.{HandledCompletely, Inner}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class HandleOrForward(message : Any, delegates : List[ActorRef] = List[ActorRef]())
private case class AddDelegate(val ref : ActorRef)
private case class IsDelegateReq()
private case class IsDelegateResp()

trait PassUnhandled extends Actor { this : Delegate =>
  protected[this] val pass : ActorRef

  override def unhandled(message: Any) : Unit = message match {
    case HandleOrForward(m, delegates) if delegates.isEmpty => pass ! m
    case m => super.unhandled(m)
  }
}

trait UsesDelegates { this : Actor =>
  private [this] class DelegatesMonitor extends Actor {
    import context._

    def f(delegates : List[ActorRef]) : Receive = {
      case AddDelegate(ref) =>
        watch(ref)
        become(f(ref :: delegates))
      case HandleOrForward(message, _) =>
        if (!delegates.isEmpty)
          delegates.head forward HandleOrForward(message, delegates.tail)
      case Terminated(watched) => become(f(delegates.filter(_ != watched)))
    }

    override def receive: Receive = f(List[ActorRef]())
  }

  private [this] val _monitor = context.actorOf(Props(new DelegatesMonitor))

  def addDelegate(candidate : ActorRef)(implicit timeout : Timeout = Timeout(1 second)) : Future[Unit] =
    (candidate ? IsDelegateReq()).map {
      case IsDelegateResp() => _monitor ! AddDelegate(candidate)
    }

  def passToDelegates(message : Any) = _monitor forward HandleOrForward(message)
}

trait Delegate extends Actor { this : ReceivePipeline =>
  pipelineInner {
    case IsDelegateReq() =>
      sender ! IsDelegateResp()
      HandledCompletely
    case f @ HandleOrForward(m, delegates) =>
      Inner(m)
  }

  override def unhandled(message: Any): Unit = { // receives original message not altered by any pipeline
    message match {
      case HandleOrForward(m, head :: delegates) =>
        head forward HandleOrForward(m, delegates)
      case HandleOrForward(_, _) =>
      case _ => super.unhandled(message)
    }
  }
}
