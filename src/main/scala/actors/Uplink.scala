package actors

import COMMUNICATION.ACK
import actors.Uplink._
import akka.actor.{Actor, ActorRef, Props, Stash}

object Uplink {
  case class Acknowledgement(transactionTag : Int, ack : ACK)
  case class GetTransactionIdsReq()
  case class GetTransactionIdsResp(ids : Set[Int])
  trait UplinkMessage

  type TransactionId = Int
  class TranslationCase(val message : (TransactionId) => com.trueaccord.scalapb.GeneratedMessage,
                        val response : (ACK) => Any) // generate response to upper layer based on ACK
  type Translation = PartialFunction[UplinkMessage, TranslationCase]

  def props(connector : ActorRef, doTranslation : Translation) =
    Props(new Uplink(connector, doTranslation))
}

class Uplink (connector : ActorRef, doTranslation : Translation)
  extends Actor with Stash {
  import context._

  var client : ActorRef = null

  connector ! GetTransactionIdsReq()

  def become1(ids : Set[TransactionId], acks : Map[TransactionId, (ACK) => Any]) = {
    assert(ids.nonEmpty)
    become(handleAck(ids, acks) orElse {
      case x : UplinkMessage => (doTranslation andThen f(ids, acks))(x)
    })
  }

  def waitingForTransactionIds(acks : Map[TransactionId, (ACK) => Any]) : Receive = {
    case GetTransactionIdsResp(ids) =>
      unstashAll()
      become1(ids, acks)
    case x : UplinkMessage => stash()
  }

  def handleAck(ids : Set[TransactionId], acks : Map[TransactionId, (ACK) => Any]) : Receive = {
    case Acknowledgement(transactionTag, ack) =>
      acks.get(transactionTag) match {
        case Some(handler) =>
          assert(client != null)
          client ! handler(ack)
          become1(ids, acks - transactionTag)
        case None => // ack is not targeted at this uplink
      }
  }

  def f(ids : Set[TransactionId], acks : Map[TransactionId, (ACK) => Any]) : PartialFunction[TranslationCase, Unit] = {
    case tc =>
      client = sender
      val id = ids.head
      connector ! tc.message(id)

      if (ids.tail.isEmpty) {
        connector ! GetTransactionIdsReq()
        become(waitingForTransactionIds(acks + (id -> tc.response)))
      } else {
        become1(ids.tail, acks + (id -> tc.response))
      }
  }

  final override def receive: Receive = waitingForTransactionIds(Map[TransactionId, (ACK) => Any]())
}
