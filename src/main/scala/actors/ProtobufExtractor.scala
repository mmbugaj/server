package actors

import COMMUNICATION.ANY_MESSAGE
import actors.ProtobufExtractor.Burst
import akka.actor.{Actor, ActorRef, Props}
import akka.util.ByteString
import util.Collector

private class ProtobufExtractor(connector : ActorRef) extends Actor {
  def extractConcreteMessageAndSendToHandler(data : ByteString) : Unit = {
    val any = ANY_MESSAGE.parseFrom(data.toArray)
    def send(msg: com.trueaccord.scalapb.GeneratedMessage) : Unit = {
      connector ! ProtobufExtractor.Message(any.transactionTag.transactionTag, msg)
    }
    any.ack.foreach(send)
    any.registerRequest.foreach(send)
    any.leave.foreach(send)
    any.pwmSetChannelRequest.foreach(send)
  }

  var collector = new Collector(
    extractConcreteMessageAndSendToHandler,
    0, ByteString())

  override def receive = {
    case Burst(data) => collector = collector(data)
  }
}

private object ProtobufExtractor {
  case class Burst(data : ByteString)
  case class Message(transactionTag : Int, msg : com.trueaccord.scalapb.GeneratedMessage)
  def props(connector : ActorRef) = Props(new ProtobufExtractor(connector))
}