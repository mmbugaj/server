package actors

import COMMUNICATION.PWM_SET_CHANNEL_REQUEST.CHANGE_TYPE.CHANGE_TYPE_IMMEDIATE
import COMMUNICATION._
import actors.DeviceManager.InterfaceToDevice.NewDevice
import actors.ProtobufExtractor.Burst
import actors.Uplink._
import akka.actor.SupervisorStrategy.Stop
import akka.actor.{Actor, ActorRef, OneForOneStrategy, Props, ReceiveTimeout, SupervisorStrategy, Terminated}
import akka.io.Tcp._
import akka.util.ByteString

import scala.concurrent.duration._

class PhysicalDeviceConnector(connection : ActorRef, generalManager : ActorRef) extends Actor {
  import context.{actorOf, become, setReceiveTimeout, stop, watch}

  val extractor = actorOf(ProtobufExtractor.props(self))
  connection ! Register(self)
  watch(extractor)
  setReceiveTimeout(5 second)

  val handleTcpConnection : Receive = {
    case Received(data) => extractor ! Burst(data)
    case message : ANY_MESSAGE =>
      println(s"Writing message $message")
      connection ! Write(ByteString(message.toByteArray.length.toByte) ++ ByteString(message.toByteArray))
    case _ : ConnectionClosed => stop(self)
    case Terminated(extractor) => stop(self)
  }

  def deviceConnected(uplinks : Seq[ActorRef], freeTransactionId : Int) : Receive = {
    case ProtobufExtractor.Message(transactionTag, ack @ ACK(_, _)) =>
      println(s"Received ack for tag $transactionTag : $ack")
      uplinks.foreach(_ ! Uplink.Acknowledgement(transactionTag, ack))
    case GetTransactionIdsReq() =>
      val burst = 10
      sender ! GetTransactionIdsResp((freeTransactionId until freeTransactionId + burst).toSet)
      become(deviceConnected(uplinks, freeTransactionId + burst) orElse handleTcpConnection)
  }

  val init : Receive = {
    case ReceiveTimeout => stop(self)
    case ProtobufExtractor.Message(transactionTag, REGISTER_REQUEST(name, physicalDeviceId, description, channelList)) =>
      setReceiveTimeout(Duration.Undefined)

      val uplinks = channelList.map(info => {
        val (translation, factory) = PhysicalDeviceConnector.provider(info)
        val uplink = actorOf(Uplink.props(self, translation))
        val hash = physicalDeviceId | (info.id << 8)
        generalManager ! NewDevice(hash, actorOf(factory(uplink, hash)))
        uplink
      })

      become(deviceConnected(uplinks, 0) orElse handleTcpConnection)
  }

  override def receive : Receive = init orElse handleTcpConnection

  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
    case _ if sender == extractor => Stop
    case t => super.supervisorStrategy.decider(t)
  }
}

object PhysicalDeviceConnector {
  case class SetChannelReq(value : Int) extends UplinkMessage

  abstract class LogicalActorFactory() {
    def apply(uplink : ActorRef, hash : Int) : Props
  }
  type DeviceDefinitionProvider = PartialFunction[com.trueaccord.scalapb.GeneratedMessage, (Translation, LogicalActorFactory)]

  val provider : DeviceDefinitionProvider = {
    case PWM_CHANNEL_INFO(name, id) =>
      ({
        case PhysicalDeviceConnector.SetChannelReq(value) =>
          val request = PWM_SET_CHANNEL_REQUEST(id, value, CHANGE_TYPE_IMMEDIATE)
          new TranslationCase(
            transactionTag => ANY_MESSAGE(TRANSACTION_TAG(transactionTag), pwmSetChannelRequest = Some(request)),
            ack => PWMChannelActor.SetChannelResp(Some(Unit)))
      },
        new LogicalActorFactory {
          override def apply(uplink : ActorRef, hash : Int): Props = PWMChannelActor.props(uplink, hash, name)
        })
  }

  def props(connection : ActorRef, generalManager : ActorRef) =
    Props(new PhysicalDeviceConnector(connection, generalManager))
}
