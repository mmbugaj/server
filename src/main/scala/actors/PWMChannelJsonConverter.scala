package actors

import actors.DeviceActor._
import actors.PWMChannelActor.{PWMChannelProperties, PWMChannelState}
import akka.actor.{Actor, ActorRef, Props}
import akka.contrib.pattern.ReceivePipeline
import akka.pattern._
import akka.util.Timeout
import spray.json._
import util.ResponsibilityChain.Delegate

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

sealed class PWMChannelJsonConverter(master : ActorRef) extends Actor with ReceivePipeline with Delegate {
  import DefaultJsonProtocol._
  import context._
  implicit val timeout: Timeout = Timeout(3 second)

  def conversion(implicit request : Request) : Unit = {
    def c : PartialFunction[Any, Future[Any]] = {
      case GetPropertiesInJsonReq() =>
        (master ? GetPropertiesReq()).mapTo[GetPropertiesResp[PWMChannelProperties]]
          .map(response =>
            JsObject(
            "type" -> JsString("PWM"),
            "name" -> JsString(response.properties.name)))
          .map(GetPropertiesInJsonResp(_))

      case GetStateInJsonReq() =>
        (master ? GetStateReq()).mapTo[GetStateResp[PWMChannelState]]
          .map(response => GetStateInJsonResp(JsObject("value" -> response.state.map(_.value).toJson)))

      case SetStateFromJsonReq(jsValue) =>
        Try {
          jsValue.asJsObject.getFields("value").head.convertTo[Int]
        } match {
          case Success(value) =>
            (master ? SetStateReq(PWMChannelState(value))).mapTo[SetStateResp]
              .map(response => SetStateFromJsonResp(response.status))
          case Failure(f) => Future.failed(new RuntimeException())
        }
    }
    c(request).pipeTo(sender)(master)
  }

  override def receive: Receive = {
    case r : Request => conversion(r)
  }
}

object PWMChannelJsonConverter {
  def props(master : ActorRef) = Props(new PWMChannelJsonConverter(master))
}