package actors

import actors.DeviceActor._
import actors.PWMChannelActor.{PWMChannelProperties, PWMChannelState, SetChannelResp}
import akka.actor.{ActorRef, Props, ReceiveTimeout}
import util.ResponsibilityChain._
import scala.concurrent.duration._

trait MsgIdentifierTag {
  val id : Option[Int] = None
}

trait IdProvider {
  var idCounter : Int = 0

  trait MsgIdentifier extends MsgIdentifierTag {
    private val _id = idCounter
    idCounter = idCounter + 1
    override val id: Option[Int] = Some(_id)
  }
}

sealed class PWMChannelActor(_connector : ActorRef, _hash : Int, _name : String)
  extends DeviceActor(_connector, _hash, _name) with UsesDelegates {
  import context._

  addDelegate(actorOf(PWMChannelJsonConverter.props(self)))

  val properties : PWMChannelProperties = PWMChannelProperties(_hash, _name)

  def idle(currentValue : Option[Int]) : Receive = {
    case r : Request =>
      implicit val _ = r
      r match {
      case GetPropertiesReq() => sender ! GetPropertiesResp(properties)
      case GetStateReq() => sender ! GetStateResp(currentValue.map(PWMChannelState(_)))
      case SetStateReq(PWMChannelState(value)) =>
        _connector ! PhysicalDeviceConnector.SetChannelReq(value) // timeout for response
        become(setChannelReqSent(value, sender)(r))
        setReceiveTimeout(3 second)
      case m => passToDelegates(m) // introduce general mechanism for passing unhandled Requests to delegates
    }
  }

  def setChannelReqSent(requestedValue : Int, listener : ActorRef)(request : Request) : Receive = {
    case r : Request =>
      implicit val _ = r
      r match {
        case GetPropertiesReq() => sender ! GetPropertiesResp(properties)
        case GetStateReq() => sender ! GetStateResp(None)
        case m => passToDelegates(m)
      }

    case SetChannelResp(status) =>
      setReceiveTimeout(Duration.Undefined)
      listener ! SetStateResp(status)(request)
      become(idle(Some(requestedValue)))

    case ReceiveTimeout => become(idle(None))
  }

  override def receive1: Receive = idle(None)
}

object PWMChannelActor {
  case class PWMChannelState(value : Int) extends State
  case class PWMChannelProperties(hash : DeviceActor.Id, name : String) extends Properties
  case class SetChannelResp(status : Option[Unit])

  object InterfaceToDeviceManager {
    case class GetDevicesRequest()
  }

  def props(connector : ActorRef,
            channelId : Int,
            channelName : String) =
    Props(new PWMChannelActor(connector, channelId, channelName))
}
