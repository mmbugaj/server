package actors

import actors.DeviceActor.{Id, Welcome}
import actors.DeviceManager.InterfaceToDevice.NewDevice
import actors.DeviceManager.InterfaceToWebServer.RouteCommand
import actors.PWMChannelActor.InterfaceToDeviceManager.GetDevicesRequest
import akka.actor.{Actor, ActorRef, PoisonPill, Props, Terminated}
import controllers.WebServer.InterfaceToDeviceManager.{GetDevicesResponse, NoSuchDevice}

class DeviceManager() extends Actor {
  import context._

  type ActorRef2Id = Map[ActorRef, Id]
  type Id2ActorRef = Map[Id, ActorRef]

  def f(map1 : ActorRef2Id, map2 : Id2ActorRef) : Receive = {
    case NewDevice(id, ref) =>
      if (map1.contains(sender) || map2.contains(id)) {
        ref ! PoisonPill
      } else {
        ref ! Welcome()
        watch(ref)
        become(f(map1 + (ref -> id), map2 + (id -> ref)))
      }

    case RouteCommand(command, id) =>
      val device = map2.get(id)
      device.map(_ forward command)
      if (device.isEmpty) sender ! NoSuchDevice()

    case Terminated(actor) =>
      map1.get(actor).foreach(id => become(f(map1 - actor, map2 - id)))

    case GetDevicesRequest() =>
      sender ! GetDevicesResponse(map2.keys)
  }

  override def receive = f(Map[ActorRef, Id](), Map[Id, ActorRef]())
}

object DeviceManager {
  object InterfaceToDevice {
    case class NewDevice(id : DeviceActor.Id, ref : ActorRef)
  }

  object InterfaceToWebServer {
    case class RouteCommand(msg : AnyRef, id : Id)
  }

  def props = Props[DeviceManager]
}
