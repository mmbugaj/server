package actors

import actors.DeviceActor.Welcome
import akka.actor.{Actor, ActorRef, ReceiveTimeout}
import spray.json.JsValue

import scala.concurrent.duration._

abstract class DeviceActor(val _connector : ActorRef, val _hash : Int, val _name : String)
  extends Actor {
  import context._

  def receive1 : Receive

  setReceiveTimeout(1 second)
  final override def receive: Receive = {
    case Welcome() =>
      setReceiveTimeout(Duration.Undefined)
      become(receive1)
    case ReceiveTimeout => context stop self
  }
}

object DeviceActor {
  case class Welcome()
  type Id = Int
  trait Properties {
    val hash : Id
    val name : String
  }
  trait State

  trait Request
  trait Response {
    val request: Request
  }

  case class GetPropertiesReq() extends Request
  case class GetPropertiesInJsonReq() extends Request
  case class GetStateReq() extends Request
  case class GetStateInJsonReq() extends Request
  case class SetStateReq[T <: State](state : T) extends Request
  case class SetStateFromJsonReq(jsValue: JsValue) extends Request

  case class GetPropertiesResp[T <: Properties](properties : T)(implicit val request: Request) extends Response
  case class GetPropertiesInJsonResp(jsValue : JsValue)(implicit val request: Request) extends Response
  case class GetStateResp[T <: State](state : Option[T])(implicit val request: Request) extends Response
  case class GetStateInJsonResp(jsValue: JsValue)(implicit val request: Request) extends Response
  case class SetStateResp(status : Option[Unit])(implicit val request: Request) extends Response
  case class SetStateFromJsonResp(status : Option[Unit])(implicit val request: Request) extends Response
}