package actors

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, Props}
import akka.io.Tcp.{Bind, Bound, CommandFailed, Connected}
import akka.io.{IO, Tcp}
import play.api.Logger

class DeviceListener(val manager : ActorRef) extends Actor {
  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress("0.0.0.0", 12345))

  override def receive: Receive = {
    case b @ Bound(_) => Logger.info("Listener bound: " + b)
    case CommandFailed(_ : Bind) => context stop self
    case c @ Connected(remote, local) => context.actorOf(PhysicalDeviceConnector.props(sender, manager))
  }
}

object DeviceListener {
  def props(manager : ActorRef) = Props(new DeviceListener(manager))
}
