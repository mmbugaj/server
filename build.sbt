name := """play-scala"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "com.google.protobuf" % "protobuf-java" % "2.6.1",
  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.11",
  "com.typesafe.akka" %% "akka-http-spray-json-experimental" % "2.4.11",
  "com.typesafe.akka" %% "akka-http-testkit" % "2.4.11",
  "com.typesafe.akka" %% "akka-contrib" % "2.4.11",
  "org.scalatest" % "scalatest_2.11" % "3.0.0" % "test"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

fork in run := true

import com.trueaccord.scalapb.{ScalaPbPlugin => PB}
PB.protobufSettings
sourceDirectories in PB.protobufConfig := List(new java.io.File("""src/main/scala/protobuf_communication"""))
sourceDirectory in PB.protobufConfig := new java.io.File("""src/main/scala/protobuf_communication""")

mainClass in (Compile, run) := Some("Main")

